#-------------------------------------------------
#
# Project created by QtCreator 2010-07-03T12:01:15
#
#-------------------------------------------------

QT += core gui sql

TARGET = moleto
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    dbmanager.cpp \
    wordlist.cpp \
    editword.cpp \
    word.cpp \
    wordbrowser.cpp \
    editmeaning.cpp \
    addtranslation.cpp \
    addexample.cpp \
    examplelistwidget.cpp \
    quizsetupdialog.cpp \
    quizdialog.cpp \
    quizwordresult.cpp \
    quizsummary.cpp \
    editattribute.cpp \
    quickadd.cpp

HEADERS += mainwindow.h \
    dbmanager.h \
    wordlist.h \
    editword.h \
    word.h \
    wordbrowser.h \
    editmeaning.h \
    addtranslation.h \
    addexample.h \
    examplelistwidget.h \
    quizsetupdialog.h \
    quizdialog.h \
    quizwordresult.h \
    quizsummary.h \
    editattribute.h \
    quickadd.h

FORMS += mainwindow.ui \
    editword.ui \
    editmeaning.ui \
    addtranslation.ui \
    addexample.ui \
    quizsetupdialog.ui \
    quizdialog.ui \
    quizwordresult.ui \
    quizsummary.ui \
    editattribute.ui \
    quickadd.ui

TRANSLATIONS += moleto_en.ts \
    moleto_cs.ts
