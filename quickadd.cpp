#include "quickadd.h"
#include "ui_quickadd.h"

QuickAdd::QuickAdd(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QuickAdd)
{
    ui->setupUi(this);
    m_word = new Word();
    setWindowTitle(tr("Quick add word"));
}

QuickAdd::~QuickAdd()
{
    delete ui;
    delete m_word;
}

void QuickAdd::flushForm()
{
    m_word->written = ui->written->text().trimmed();
    if (! ui->translation->text().trimmed().isEmpty()) {
        Meaning *m = new Meaning(m_word);
        m->translations.append(ui->translation->text());
        m_word->meanings.append(m);
    }
}
