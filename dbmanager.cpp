#include "dbmanager.h"
#include "word.h"

#include <QtSql/QSqlDatabase>
#include <QtCore>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QSqlRelationalTableModel>

DBManager::DBManager()
    : m_db(QSqlDatabase::addDatabase("QSQLITE")),
      m_version(0)
{
}

void DBManager::open(QString path)
{
    m_db.setDatabaseName(path);
    if (!m_db.open()) {
        throw m_db.lastError();
    }
    migrate();
}

void DBManager::close()
{
    m_db.close();
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_version = 0;
}

QSqlQueryModel *DBManager::listWords(QString filter, int sortby_column)
{
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery("SELECT word.id, batch, written, pronunciation, class FROM word WHERE written LIKE '%" + filter + "%' ORDER BY " + QString::number(sortby_column));
    
    //QSqlRelationalTableModel *model = new QSqlRelationalTableModel();
    //model->setTable("word");
    //model->setRelation(0, QSqlRelation("meaning", "wordid", "translation"));
    
    model->setHeaderData(0, Qt::Horizontal, QObject::tr("ID"));
    model->setHeaderData(1, Qt::Horizontal, QObject::tr("Batch"));
    model->setHeaderData(2, Qt::Horizontal, QObject::tr("Written"));
    model->setHeaderData(3, Qt::Horizontal, QObject::tr("Pronunciation"));
    model->setHeaderData(4, Qt::Horizontal, QObject::tr("Class"));
    model->setHeaderData(5, Qt::Horizontal, QObject::tr("translation"));
    
    return model;
}

void DBManager::deleteWord(int id)
{
    QString sid = QString::number(id);
    m_db.transaction();
    m_db.exec("delete from attributes where wordid=" + sid);
    m_db.exec("delete from meaning_translations where meaningid in (select id from meaning where wordid=" + sid + ")");
    m_db.exec("delete from examples where meaningid in (select id from meaning where wordid=" + sid + ")");
    m_db.exec("delete from meaning where wordid=" + sid);
    m_db.exec("delete from word where id=" + sid);
    m_db.exec("update example_texts set wordid=NULL where wordid=" + sid);
    m_db.commit();
}

Word *DBManager::getWord(int wordId)
{
    if (! wordId)
        return 0;

    Word *w = new Word(wordId);
    QSqlQuery q = m_db.exec("select * from word where id=" + QString::number(wordId));
    while (q.next()) {
        w->written = q.value(1).toString();
        w->pronunciation = q.value(2).toString();
        w->wordClass = q.value(3).toString();
        w->batch = q.value(4).toInt();
        w->lesson = q.value(5).toString();
        w->textbook = q.value(6).toString();
        w->created = q.value(7).toDateTime();
        w->last_quiz = q.value(8).toDateTime();
        w->last_result = q.value(9).toString();
        w->quizes = q.value(10).toInt();
        w->mistakes = q.value(11).toInt();
        w->comment = q.value(12).toString();

        QSqlQuery m = m_db.exec("select id from meaning where wordid=" + QString::number(w->id));
        qDebug() << m.lastQuery();
        while (m.next()) {
            Meaning *meaning = getMeaning(w, m.value(0).toInt());
            w->meanings.append(meaning);
        }

        QSqlQuery a = m_db.exec("select key, value from attributes where wordid=" + QString::number(w->id) + " order by position asc");
        qDebug() << a.lastQuery();
        while (a.next()) {
            w->attributes.insert(a.value(0).toString(), a.value(1).toString());
        }
    }
    return w;
}

Meaning *DBManager::getMeaning(Word *w, int meaningId)
{
    Meaning *meaning = new Meaning(w);
    
    QSqlQuery m = m_db.exec("select definition from meaning where id=" + QString::number(meaningId));
    while (m.next()) { 
        meaning->definition = m.value(0).toString();
        
        QSqlQuery t = m_db.exec("select translation from meaning_translations where meaningid=" + QString::number(meaningId));
        qDebug() << t.lastQuery();
        while (t.next()) {
            meaning->translations.append(t.value(0).toString());
        }
        
        QSqlQuery e = m_db.exec("select original, translation from examples where meaningid=" + QString::number(meaningId));
        qDebug() << e.lastQuery();
        while (e.next()) {
            meaning->examples.append(Example(e.value(0).toString(), e.value(1).toString()));
        }
    }
    
    return meaning;
}

bool DBManager::saveWord(Word *word, bool transaction)
{
    qDebug() << "saveWord";
    
    QSqlQuery q;

    if (transaction)
        m_db.transaction();
    
    if (word->id > 0) {
        q.prepare("update word set written=:written, pronunciation=:pronunciation, class=:class, batch=:batch, lesson=:lesson, textbook=:textbook, last_result=:last_result, last_quiz=:last_quiz, quizes=:quizes, mistakes=:mistakes, comment=:comment where id=" + QString::number(word->id));
    } else {
        q.prepare("insert into word (written, pronunciation, class, batch, lesson, textbook, last_result, last_quiz, quizes, mistakes, comment) values (:written, :pronunciation, :class, :batch, :lesson, :textbook, :last_result, :last_quiz, :quizes, :mistakes, :comment)");
    }
    
    q.bindValue(":written", word->written);
    q.bindValue(":pronunciation", word->pronunciation);
    q.bindValue(":class", word->wordClass);
    q.bindValue(":batch", word->batch);
    q.bindValue(":lesson", word->lesson);
    q.bindValue(":textbook", word->textbook);
    q.bindValue(":last_result", word->last_result);
    q.bindValue(":last_quiz", word->last_quiz);
    q.bindValue(":quizes", word->quizes);
    q.bindValue(":mistakes", word->mistakes);
    q.bindValue(":comment", word->comment);
    q.exec();
    
    if (word->id <= 0) {
        QSqlQuery lastq = m_db.exec("select last_insert_rowid()");
        lastq.next();
        word->id = lastq.value(0).toInt();
    }
    //qDebug() << q.lastError().text();
    qDebug() << q.executedQuery();

    // clean old values
    m_db.exec("delete from attributes where wordid=" +QString::number(word->id));
    m_db.exec("delete from meaning_translations where meaningid in (select id from meaning where wordid=" + QString::number(word->id) + ")");
    m_db.exec("delete from examples where meaningid in (select id from meaning where wordid=" + QString::number(word->id) + ")");
    m_db.exec("delete from meaning where wordid=" + QString::number(word->id));

    // save attributes
    QMap<QString, QString>::iterator ait = word->attributes.begin();
    while (ait != word->attributes.end()) {
        QSqlQuery aq;
        aq.prepare("insert into attributes (wordid, key, value) values(:wordid, :key, :value)");
        aq.bindValue(":wordid", word->id);
        aq.bindValue(":key", ait.key());
        aq.bindValue(":value", ait.value());
        if (! aq.exec() )
            qDebug() << "Error inserting attribute";
        qDebug() << aq.lastQuery();
        ++ait;
    }

    // save meanings
    QList<Meaning *>::iterator it = word->meanings.begin();
    while (it != word->meanings.end()) {
        // save definition
        QSqlQuery mq;
        mq.prepare("insert into meaning (wordid, definition) values(:wordid, :definition)");
        mq.bindValue(":wordid", QString::number(word->id));
        mq.bindValue(":definition", (*it)->definition);
        mq.exec();
        qDebug() << mq.lastQuery();

        // get definition id
        QSqlQuery lastq = m_db.exec("select last_insert_rowid()");
        lastq.next();
        int mid = lastq.value(0).toInt();

        // save translations
        QList<QString>::iterator i = (*it)->translations.begin();
        for (; i != (*it)->translations.end(); ++i) {
            QSqlQuery tq;
            tq.prepare("insert into meaning_translations (meaningid, translation) values(:meaningid, :translation)");
            tq.bindValue(":meaningid", QString::number(mid));
            tq.bindValue(":translation", (*i));
            tq.exec();
            qDebug() << q.lastQuery();
        }

        // save examples
        QList<Example>::iterator ite = (*it)->examples.begin();
        for (; ite != (*it)->examples.end(); ++ite) {
            QSqlQuery eq;
            eq.prepare("insert into examples (meaningid, original, translation) values(:meaningid, :original, :translation)");
            eq.bindValue(":meaningid", QString::number(mid));
            eq.bindValue(":original", (*ite).original);
            eq.bindValue(":translation", (*ite).translation);
            eq.exec();
            qDebug() << q.lastQuery();
        }
        ++it;
    }

    if (transaction)
        m_db.commit();

    return true;
}

bool DBManager::saveWords(QList<Word *> & words)
{
    QList<Word*>::iterator it = words.begin();

    m_db.transaction();
    for (; it != words.end(); it++) {
        saveWord( *it, false );
    }
    return m_db.commit();
}

QStringList DBManager::list(QString field) const
{
    QStringList list;

    QSqlQuery q = m_db.exec("SELECT DISTINCT " + field + " FROM word ORDER BY " + field + " ASC");

    list.append("");
    while (q.next()) {
        QString str = q.value(0).toString().trimmed();
        if (!str.isEmpty())
            list.append(q.value(0).toString());
    }

    return list;
}

QList<Word*> DBManager::getQuizWords()
{
    QList<Word*> list;

    QSqlQuery q = m_db.exec("SELECT id FROM word ORDER BY RANDOM()");
    while (q.next()) {
        Word *w = getWord(q.value(0).toInt());
        if (w->isQuizWord())
            list.append(w);
    }

    return list;
}

// create and update db tables
void DBManager::migrate()
{
    int latest_version = 2;
    
    // get latest db version
    if (m_db.tables().size() > 0) {
        QSqlQuery q = m_db.exec("select * from version limit 1");
        q.next();
        m_version = q.value(0).toInt();
        // same as latest => return
        if (latest_version == m_version) {
            return;
        }
    }

    // init db
    if (m_version == 0) {
        qDebug() << "init db";
        
        // create database
        m_db.exec("create table version( version int )");
        m_db.exec("insert into version values(0)");
        
        // table word
        m_db.exec("create table word(id integer primary key, written varchar, pronunciation varchar, class varchar, batch integer, lesson varchar, textbook varchar, created timestamp default current_timestamp, last_quiz timestamp, last_result int, quizes int, mistakes int)");
        m_db.exec("create index word_written on word('written')");
        m_db.exec("create index word_written_class on word('written', 'class')");
        
        // table attributes
        m_db.exec("create table attributes(id integer primary key, wordid integer references word on delete cascade, position int, key varchar, value text)");
        m_db.exec("create index attributes_key on attributes('word')");
        m_db.exec("create index attributes_key_position on attributes('word', 'position')");
        
        // table meaning
        m_db.exec("create table meaning(id integer primary key, wordid intenger references word on delete cascade, position integer, definition text)");
        m_db.exec("create table meaning_translations(id integer primary key, meaningid integer references meaning on delete cascade, position integer, translation text)");
        
        // table example
        m_db.exec("create table examples(id integer primary key, meaningid integer references meaning on delete cascade, original text, translation text)");

        ++m_version;
    }

    if (m_version == 1) {
        qDebug() << "migrate to db version " << (m_version + 1);

        // add field comment
        m_db.exec("alter table word add column comment text");
        ++m_version;
    }
    
    // save updated db version
    qDebug() << "update db to version: " << QString::number(m_version);
    m_db.transaction();
    m_db.exec("update version set version=" + QString::number(m_version));
    m_db.commit();
}
