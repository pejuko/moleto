#include "wordlist.h"

WordList::WordList(QWidget *parent) :
    QTableView(parent)
{
}

WordList::~WordList()
{
    delete model();
}

int WordList::currentID()
{
    return model()->index(currentIndex().row(), 0).data().toInt();
}

QString WordList::currentWord()
{
    return model()->index(currentIndex().row(), 2).data().toString();
}

void WordList::selectWord(Word *w)
{
    int row;
    for (row=0; row < model()->rowCount(); row++) {
        int row_id = model()->index(row, 0).data().toInt();
        if (w->id == row_id)
            break;
    }
    selectRow(row);
}

void WordList::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
    QTableView::currentChanged(current, previous);
    emit wordChanged();
}
