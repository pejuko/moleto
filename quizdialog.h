#ifndef QUIZDIALOG_H
#define QUIZDIALOG_H

#include <QDialog>

#include "dbmanager.h"
#include "quizsetupdialog.h"

namespace Ui {
class QuizDialog;
class QuizSetupDialog;
}

class QuizDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit QuizDialog(QWidget *parent = 0, DBManager *dbm=0, QuizSetupDialog::Mode mode=QuizSetupDialog::Normal, QString textbook="");
    ~QuizDialog();

    bool hasWords() const { return m_words.size() > 0; }

public slots:
    void showWord();
    void knownWord();
    void unknownWord();
    void save();

private:
    Ui::QuizDialog *ui;
    DBManager *db;
    QuizSetupDialog::Mode m_mode;
    QString m_textbook;
    QList<Word*> m_words;
    QList<Word*> m_unknownWords;
    QList<Word*>::iterator m_it;
    int m_known;
    int m_unknown;
    bool m_repeat;
    int m_actKnown;
    int m_actUnknown;
    int m_actWords;

    void nextWord();
    void nextUnknown();
};

#endif // QUIZDIALOG_H
