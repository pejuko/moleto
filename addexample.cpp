#include "addexample.h"
#include "ui_addexample.h"

AddExample::AddExample(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddExample)
{
    ui->setupUi(this);
}

AddExample::~AddExample()
{
    delete ui;
}

QString AddExample::original() const
{
    return ui->original->text();
}

void AddExample::setOriginal(QString value)
{
    ui->original->setText(value);
}

QString AddExample::translation() const
{
    return ui->translation->text();
}

void AddExample::setTranslation(QString value)
{
    ui->translation->setText(value);
}
