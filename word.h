#ifndef WORD_H
#define WORD_H

#include <QList>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include <QMultiMap>

struct Word;
struct Example;

struct Meaning
{
    Meaning(Word *w);
    
    int id;
    Word *p_word;
    QString definition;
    QStringList translations;
    QList<Example> examples;

    QString examplesToHtml();
    QString translationsToHtml(bool bExamples=true);
};

struct Word
{
    Word(int wid=-1);
    ~Word();
    
    bool isQuizWord();
    int daysToNextQuiz();

    QString toHtml();
    QString htmlStyle();
    QString htmlHeader();
    QString propertiesToHtml();
    QString meaningsToHtml(bool bExamples=true);

    bool compare(QString const& str) {
        return written.trimmed() == str.trimmed();
    }
    
    int id;
    QString written;
    QString pronunciation;
    QString wordClass;
    int batch;
    QString lesson;
    QString textbook;
    QDateTime created;
    QDateTime last_quiz;
    QString last_result;
    int quizes;
    int mistakes;
    QList<Meaning*> meanings;
    QMap<QString, QString> attributes;
    QString comment;
};

struct Example
{
    Example(int wid=-1);
    Example(QString orig, QString transl);
    
    int id;
    QString original;
    QString translation;
};


#endif // WORD_H
