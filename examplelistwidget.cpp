#include "examplelistwidget.h"
#include "word.h"

ExampleListWidget::ExampleListWidget(QWidget *parent) :
    QListWidget(parent)
{
}

void ExampleListWidget::addItems(Meaning *meaning)
{
    QList<Example>::iterator it = meaning->examples.begin();
    for(; it != meaning->examples.end(); it++) {
        QString itm = (*it).original;
        //itm += "<br><i>(";
        itm += " (";
        itm += (*it).translation;
        itm += ")";
        //itm += ")</i>";
        addItem(itm);
    }
}
