#ifndef EDITATTRIBUTE_H
#define EDITATTRIBUTE_H

#include <QDialog>
#include <QString>

namespace Ui {
class EditAttribute;
}

class EditAttribute : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditAttribute(QWidget *parent = 0);
    EditAttribute(QWidget *parent, QString key, QString value);

    ~EditAttribute();

    QString key();
    QString value();
    
private:
    Ui::EditAttribute *ui;
};

#endif // EDITATTRIBUTE_H
