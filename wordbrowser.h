#ifndef WORDBROWSER_H
#define WORDBROWSER_H

#include <QTextBrowser>
#include <word.h>

class WordBrowser : public QTextBrowser
{
    Q_OBJECT
public:
    explicit WordBrowser(QWidget *parent = 0);
    
signals:
    
public slots:
    void updateWord(Word *w);
    
};

#endif // WORDBROWSER_H
