#ifndef EDITWORD_H
#define EDITWORD_H

#include "dbmanager.h"
#include "word.h"

#include <QDialog>

namespace Ui {
class EditWord;
}

class EditWord : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditWord(QWidget *parent = 0, Word *w=0, DBManager *dbm=0);
    ~EditWord();
    
    Word *word() { return p_word; };

public slots:
    void flushForm();
    void addMeaning();
    void editMeaning();
    void deleteMeaning();
    void addAttribute();
    void editAttribute();
    void deleteAttribute();
    
protected:
	void fillMeanings();
	void fillAttributes();
    void fillForm();
    
private:
    Ui::EditWord *ui;
    DBManager *db;
    Word *p_word;
};

#endif // EDITWORD_H
