#ifndef ADDEXAMPLE_H
#define ADDEXAMPLE_H

#include <QDialog>
#include <QString>

namespace Ui {
class AddExample;
}

class AddExample : public QDialog
{
    Q_OBJECT
    
public:
    explicit AddExample(QWidget *parent = 0);
    ~AddExample();
    
    QString original() const;
    void setOriginal(QString value);
    
    QString translation() const;
    void setTranslation(QString value);
    
private:
    Ui::AddExample *ui;
};

#endif // ADDEXAMPLE_H
