#include "word.h"
#include "dbmanager.h"
#include "editmeaning.h"
#include "addtranslation.h"
#include "addexample.h"

#include "ui_editmeaning.h"

#include <QtCore>
#include <QMessageBox>

EditMeaning::EditMeaning(QWidget *parent, Meaning *m, DBManager *dbm) :
    QDialog(parent),
    ui(new Ui::EditMeaning),
    db(dbm),
    p_meaning(m)
{
    ui->setupUi(this);
    fillForm();
}

EditMeaning::~EditMeaning()
{
    delete ui;
}

void EditMeaning::fillForm()
{
    ui->definition->setPlainText(p_meaning->definition);
    
    ui->translations->clear();
    ui->translations->addItems(p_meaning->translations);

    ui->examplesList->clear();
    ui->examplesList->addItems(p_meaning);
}


void EditMeaning::flushForm()
{
    p_meaning->definition = ui->definition->toPlainText();
    
    p_meaning->translations.clear();
    for (int i=0; i<ui->translations->count(); i++) {
        p_meaning->translations.append(ui->translations->item(i)->text());
    }
}

void EditMeaning::addTranslation()
{
    AddTranslation at;
    at.show();
    flushForm();
    if (at.exec()) {
        p_meaning->translations.append(at.translation());
        fillForm();
    }
}

void EditMeaning::editTranslation()
{
    int row = ui->translations->currentRow();
    if (row < 0)
        return;

    AddTranslation at;
    at.setTranslation(ui->translations->currentItem()->text());
    at.show();
    flushForm();
    if (at.exec()) {
        p_meaning->translations.replace(row, at.translation());
        fillForm();
    }
}

void EditMeaning::deleteTranslation()
{
    int row = ui->translations->currentRow();
    if (row < 0)
        return;

    QString msg = tr("<b>Delete</b> translation: ") + ui->translations->currentItem()->text() + "?";

    flushForm();

    if (QMessageBox::question(this, tr("Delete Translation?"), msg, QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {
        p_meaning->translations.removeAt(row);
        fillForm();
    }
}

void EditMeaning::addExample()
{
    AddExample ae;
    ae.show();
    flushForm();
    if (ae.exec()) {
        p_meaning->examples.append(Example(ae.original(), ae.translation()));
        fillForm();
    }
}

void EditMeaning::editExample()
{
    int row = ui->examplesList->currentRow();
    if (row < 0)
        return;

    AddExample ae;
    QString o = p_meaning->examples.at(row).original;
    QString t = p_meaning->examples.at(row).translation;
    ae.setOriginal(o);
    ae.setTranslation(t);
    ae.show();
    flushForm();
    if (ae.exec()) {
        p_meaning->examples.replace(row, Example(ae.original(), ae.translation()));
        fillForm();
    }
}

void EditMeaning::deleteExample()
{
    int row = ui->examplesList->currentRow();
    if (row < 0)
        return;
    QString msg = tr("<b>Delete</b> example: ") + ui->examplesList->currentItem()->text() + "?";
    
    flushForm();
    if (QMessageBox::question(this, tr("Delete Example?"), msg, QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {
        p_meaning->examples.removeAt(row);
        fillForm();
    }
}
