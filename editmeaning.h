#ifndef EDITMEANING_H
#define EDITMEANING_H

#include "word.h"
#include "dbmanager.h"

#include <QDialog>

namespace Ui {
class EditMeaning;
}

class EditMeaning : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditMeaning(QWidget *parent = 0, Meaning *m=0, DBManager *dbm=0);
    ~EditMeaning();
    
    void fillForm();
    void flushForm();
    
public slots:
    void addTranslation();
    void editTranslation();
    void deleteTranslation();
    
    void addExample();
    void editExample();
    void deleteExample();
    
private:
    Ui::EditMeaning *ui;
    DBManager *db;
    Meaning *p_meaning;
};

#endif // EDITMEANING_H
