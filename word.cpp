#include <QDebug>

#include <cmath>

#include "word.h"

Word::Word(int wid) :
    id(wid),
    batch(0),
    quizes(0),
    mistakes(0)
{
}

Word::~Word()
{
    while(meanings.size() > 0) { delete meanings.takeFirst(); };
}

bool Word::isQuizWord()
{
    return (batch == 0 || daysToNextQuiz() <= 0);
}

int Word::daysToNextQuiz()
{
    if (batch <= 0)
        return 0;

    int daysto = last_quiz.daysTo(QDateTime::currentDateTime());
    int power = batch - 1;
    int days = std::pow(2.0, power) - daysto;

    return std::max(days, 0);
}

QString Word::toHtml()
{
    QString html;
    
    html += htmlStyle();
    html += htmlHeader();
    html += meaningsToHtml();
    html += propertiesToHtml();

    qDebug() << html.toAscii();

    return html;
}

QString Word::htmlStyle()
{
    QString html;

    html += "<style>"
            "table {"
            "margin-top: 10px;"
            "}\n"
            ".pronunciation {"
            "color: #0000ff;"
            "}\n"
            "</style>";

    return html;
}

QString Word::htmlHeader()
{
    QString html;

    // title
    html += "<h1>" + written;
    if (! pronunciation.isEmpty())
        html += " <span class=\"pronunciation\">[" + pronunciation + "]</span>";
    html += "</h1>";

    // list of attributes
    html += "<div class=\"attributes\">";
    html += QObject::tr("Class") + ": " + wordClass;
    QMap<QString, QString>::iterator ait = attributes.begin();
    for (; ait != attributes.end(); ait++) {
        html += "; " + ait.key() + ": " + ait.value();
    }
    html += "</div>";

    // comment
    html += "<i>";
    html += comment;
    html += "</i>";

    return html;
}

QString Word::propertiesToHtml()
{
    QString html;

    html += "<table width=\"100%\"><tr>";

    html += "<td><table>";
    html += "<tr><td>" + QObject::tr("Batch") + ":</td><td>" + QString::number(batch) + "</td></tr>";
    //html += "<tr><td>" + QObject::tr("Last quiz result") + ":</td><td>" + last_result + "</td></tr>";
    html += "<tr><td>Known/Quized:</td><td>" + QString::number(quizes - mistakes) + "/" + QString::number(quizes) + "</td></tr>";
    html += "<tr><td>Last quized:</td><td>" + last_quiz.toString() + "</td></tr>";
    html += "<tr><td colspan=2>Next quiz in <b>" + QString::number(daysToNextQuiz()) + "</b> day(s)</td></tr>";
    html += "</table></td>";

    html += "<td align=\"left\"><table>";
    html += "<tr><td>Textbook:</td><td>" + textbook + "</td></tr>";
    html += "<tr><td>Lesson:</td><td>" + lesson + "</td></tr>";
    html += "<tr><td>Word added:</td><td>" + created.toString() + "</td></tr>";
    html += "</table></td>";

    html += "</tr></table>";

    return html;
}

QString Word::meaningsToHtml(bool bExamples /* true */)
{
    QString html =
            "<style>\n"
            "ul,ol,li { margin: 0px; }\n"
            "ul,ol { margin-top: 10px; margin-bottom: 10px; }\n"
            "</style>";

    if (meanings.size() > 0) {
        html += "<ol>";
        QList<Meaning*>::iterator it = meanings.begin();
        for (; it != meanings.end(); it++) {
            Meaning *m = (*it);

            html += "<li>";
            html += m->definition;
            html += m->translationsToHtml(bExamples);
            if (bExamples)
                html += m->examplesToHtml();
            html += "</li>";
        }
        html += "</ol>";
    }

    return html;
}

Meaning::Meaning(Word *w) :
    id(-1),
    p_word(w)
{
}

// if examples => (tr1, tr2, ...)
// else => * tr1
//         * tr2
//         ...
QString Meaning::translationsToHtml(bool bExamples /* true */)
{
    QString html = "";

    if (translations.size() <= 0)
        return html;

    if (bExamples)
        html += " (";
    else
        html += "<ul>";

    QList<QString>::iterator i = translations.begin();
    for(; i != translations.end(); ++i) {
        if (bExamples) {
            if (i != translations.begin())
                html += ", ";
        } else {
            html += "<li>";
        }

        html += "<b>" + (*i).trimmed() + "</b>";

        if (!bExamples)
            html += "</li>";
    }

    if (bExamples)
        html += ")";
    else
        html += "</ul>";

    return html;
}

QString Meaning::examplesToHtml()
{
    QString html = "";

    if (examples.size() <= 0)
        return html;

    html += "<ul>";
    QList<Example>::iterator ie = examples.begin();
    for (; ie != examples.end(); ++ie) {
        html += "<li><i>";
        html += (*ie).original.trimmed();
        if ((*ie).translation.trimmed() != "") {
            html += " (";
            html += (*ie).translation.trimmed();
            html += ")";
        }
        html += "</i></li>";
    }
    html += "</ul>";

    return html;
}

Example::Example(int wid) :
    id(wid)
{
}

Example::Example(QString orig, QString transl) :
    id(-1),
    original(orig),
    translation(transl)
{
}
