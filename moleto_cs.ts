<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ">
<context>
    <name>AddExample</name>
    <message>
        <location filename="addexample.ui" line="14"/>
        <source>Add new example</source>
        <translation>Přidat nový příklad</translation>
    </message>
    <message>
        <location filename="addexample.ui" line="22"/>
        <source>Original</source>
        <translation>Originál</translation>
    </message>
    <message>
        <location filename="addexample.ui" line="36"/>
        <source>Translation</source>
        <translation>Překlad</translation>
    </message>
</context>
<context>
    <name>AddTranslation</name>
    <message>
        <location filename="addtranslation.ui" line="14"/>
        <source>Add new translation</source>
        <translation>Přidat nový překlad</translation>
    </message>
</context>
<context>
    <name>EditAttribute</name>
    <message>
        <location filename="editattribute.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editattribute.ui" line="22"/>
        <source>Key</source>
        <translation>Klíč</translation>
    </message>
    <message>
        <location filename="editattribute.ui" line="36"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
</context>
<context>
    <name>EditMeaning</name>
    <message>
        <location filename="editmeaning.ui" line="14"/>
        <source>Edit meaning</source>
        <translation>Upravit význam</translation>
    </message>
    <message>
        <location filename="editmeaning.ui" line="32"/>
        <source>Definition</source>
        <translation>Definice</translation>
    </message>
    <message>
        <location filename="editmeaning.ui" line="57"/>
        <source>Translations</source>
        <translation>Překlady</translation>
    </message>
    <message>
        <location filename="editmeaning.ui" line="81"/>
        <location filename="editmeaning.ui" line="118"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="editmeaning.ui" line="88"/>
        <location filename="editmeaning.ui" line="125"/>
        <source>Edit</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="editmeaning.ui" line="95"/>
        <location filename="editmeaning.ui" line="132"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="editmeaning.ui" line="107"/>
        <source>Examples</source>
        <translation>Příklady</translation>
    </message>
    <message>
        <location filename="editmeaning.cpp" line="75"/>
        <source>&lt;b&gt;Delete&lt;/b&gt; translation: </source>
        <translation>&lt;b&gt;Smazat&lt;/b&gt; překlad: </translation>
    </message>
    <message>
        <location filename="editmeaning.cpp" line="78"/>
        <source>Delete Translation?</source>
        <translation>Smazat překlad?</translation>
    </message>
    <message>
        <location filename="editmeaning.cpp" line="113"/>
        <source>&lt;b&gt;Delete&lt;/b&gt; example: </source>
        <translation>&lt;b&gt;Smazat&lt;/b&gt; příklad: </translation>
    </message>
    <message>
        <location filename="editmeaning.cpp" line="116"/>
        <source>Delete Example?</source>
        <translation>Smazat příklad?</translation>
    </message>
</context>
<context>
    <name>EditWord</name>
    <message>
        <location filename="editword.ui" line="14"/>
        <source>Edit word</source>
        <translation>Upravit slovo</translation>
    </message>
    <message>
        <location filename="editword.ui" line="20"/>
        <source>Word description</source>
        <translation>Popis slova</translation>
    </message>
    <message>
        <location filename="editword.ui" line="30"/>
        <source>Textbook</source>
        <translation>Učebnice</translation>
    </message>
    <message>
        <location filename="editword.ui" line="54"/>
        <source>Lesson</source>
        <translation>Lekce</translation>
    </message>
    <message>
        <location filename="editword.ui" line="80"/>
        <source>Written</source>
        <translation>Psaná forma</translation>
    </message>
    <message>
        <location filename="editword.ui" line="94"/>
        <source>Pronunciation</source>
        <translation>Výslovnost</translation>
    </message>
    <message>
        <location filename="editword.ui" line="108"/>
        <source>Class</source>
        <translation>Slovní druh</translation>
    </message>
    <message>
        <location filename="editword.ui" line="126"/>
        <source>Meanings</source>
        <translation>Významy</translation>
    </message>
    <message>
        <location filename="editword.ui" line="147"/>
        <location filename="editword.ui" line="203"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="editword.ui" line="154"/>
        <location filename="editword.ui" line="210"/>
        <source>Edit</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="editword.ui" line="161"/>
        <location filename="editword.ui" line="217"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="editword.ui" line="173"/>
        <source>Attributes</source>
        <translation>Atributy</translation>
    </message>
    <message>
        <location filename="editword.ui" line="229"/>
        <source>Comment</source>
        <translation>Komentář</translation>
    </message>
    <message>
        <location filename="editword.cpp" line="49"/>
        <source>Add new meaning</source>
        <translation>Přidat nový význam</translation>
    </message>
    <message>
        <location filename="editword.cpp" line="64"/>
        <source>Edit meaning</source>
        <translation>Upravit význam</translation>
    </message>
    <message>
        <location filename="editword.cpp" line="75"/>
        <source>&lt;b&gt;Delete&lt;/b&gt; word: </source>
        <translation>&lt;b&gt;Smazaat&lt;/b&gt; slovo: </translation>
    </message>
    <message>
        <location filename="editword.cpp" line="78"/>
        <source>Delete Word?</source>
        <translation>Smazat slovo?</translation>
    </message>
    <message>
        <location filename="editword.cpp" line="170"/>
        <source>Key</source>
        <translation>Klíč</translation>
    </message>
    <message>
        <location filename="editword.cpp" line="171"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="23"/>
        <source>Filter</source>
        <translation>Filtr</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="33"/>
        <location filename="mainwindow.ui" line="194"/>
        <source>Quiz</source>
        <translation>Kvíz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="85"/>
        <source>Quick</source>
        <translation>Rychle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="92"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="99"/>
        <location filename="mainwindow.ui" line="142"/>
        <source>Edit</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="106"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="125"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="136"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="156"/>
        <source>Open</source>
        <translation>Otevřít</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="161"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="166"/>
        <source>Exit</source>
        <translation>Konec</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="169"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="174"/>
        <source>About</source>
        <translation>O programu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="179"/>
        <source>Edit word</source>
        <translation>Upravit slovíčko</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="184"/>
        <source>Add new word</source>
        <translation>Přidat slovíčko</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="189"/>
        <source>Delete word</source>
        <translation>Smazat slovíčko</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="199"/>
        <source>Quick add</source>
        <translation>Rychlé přidání</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="74"/>
        <source>about Moleto</source>
        <translation>O programu Moleto</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="74"/>
        <source>Moleto is a vocabulary learning tool by Petr Kovar (pejuko@gmail.com).</source>
        <translation>Moleto je nástroj pro učení slovíček od Petra Kováře (pejuko@gmail.com).</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="82"/>
        <source>error</source>
        <translation>chyba</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="83"/>
        <source>path</source>
        <translation>cesta</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="91"/>
        <source>Open file</source>
        <translation>Otevřít soubor</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="91"/>
        <source>moleto files (*.db);; All Files(*)</source>
        <translation>soubory moleto (*.db);; Všechny soubory(*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="100"/>
        <source>db version</source>
        <translation>verze db</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="113"/>
        <source>No words today</source>
        <translation>Dnes nejsou připravena žádná slovíčka</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="113"/>
        <source>All words were quized. Try it tomorow!</source>
        <translation>Všechna slovíčka byla otestována. Zkuste to zítra.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="140"/>
        <source>Add New Word</source>
        <translation>Přidat nové slovíčko</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="170"/>
        <source>Edit Word</source>
        <translation>Upravit slovíčko</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="187"/>
        <source>Delete Word?</source>
        <translation>Smazat slovíčko?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="187"/>
        <source>&lt;b&gt;Delete&lt;/b&gt; word: </source>
        <translation>&lt;b&gt;Smazat&lt;/b&gt; slovíčko: </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="dbmanager.cpp" line="39"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="dbmanager.cpp" line="40"/>
        <location filename="word.cpp" line="57"/>
        <source>Batch</source>
        <translation>Krabička</translation>
    </message>
    <message>
        <location filename="dbmanager.cpp" line="41"/>
        <source>Written</source>
        <translation>Psaná forma</translation>
    </message>
    <message>
        <location filename="dbmanager.cpp" line="42"/>
        <source>Pronunciation</source>
        <translation>Výslovnost</translation>
    </message>
    <message>
        <location filename="dbmanager.cpp" line="43"/>
        <location filename="word.cpp" line="49"/>
        <source>Class</source>
        <translation>Slovní druh</translation>
    </message>
    <message>
        <location filename="dbmanager.cpp" line="44"/>
        <source>translation</source>
        <translation>Překlad</translation>
    </message>
    <message>
        <location filename="word.cpp" line="58"/>
        <source>Last quiz result</source>
        <translation>Výsledek posledního kvízu</translation>
    </message>
</context>
<context>
    <name>QuickAdd</name>
    <message>
        <location filename="quickadd.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="quickadd.ui" line="20"/>
        <source>Written</source>
        <translation>Psaná forma</translation>
    </message>
    <message>
        <location filename="quickadd.ui" line="30"/>
        <source>Translation</source>
        <translation>Překlad</translation>
    </message>
    <message>
        <location filename="quickadd.cpp" line="10"/>
        <source>Quick add word</source>
        <translation>Rychlé přidání slovíčka</translation>
    </message>
</context>
<context>
    <name>QuizDialog</name>
    <message>
        <location filename="quizdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="quizdialog.ui" line="20"/>
        <source>Question</source>
        <translation>Otázka</translation>
    </message>
    <message>
        <location filename="quizdialog.ui" line="36"/>
        <source>Answer</source>
        <translation>Odpověď</translation>
    </message>
    <message>
        <location filename="quizdialog.ui" line="55"/>
        <source>I know</source>
        <translation>Vím</translation>
    </message>
    <message>
        <location filename="quizdialog.ui" line="62"/>
        <source>I don&apos;t know</source>
        <translation>Nevím</translation>
    </message>
    <message>
        <location filename="quizdialog.cpp" line="86"/>
        <location filename="quizdialog.cpp" line="154"/>
        <source>word </source>
        <translation>slovíčko </translation>
    </message>
</context>
<context>
    <name>QuizSetupDialog</name>
    <message>
        <location filename="quizsetupdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="quizsetupdialog.cpp" line="10"/>
        <source>Quiz setup</source>
        <translation>Nastavení kvízu</translation>
    </message>
    <message>
        <location filename="quizsetupdialog.ui" line="20"/>
        <source>Quiz mode</source>
        <translation>Forma kvízu</translation>
    </message>
    <message>
        <location filename="quizsetupdialog.ui" line="26"/>
        <source>Normal quiz</source>
        <translation>Normální kvíz</translation>
    </message>
    <message>
        <location filename="quizsetupdialog.ui" line="36"/>
        <source>Flash card quiz</source>
        <translation>Kartičky</translation>
    </message>
    <message>
        <location filename="quizsetupdialog.ui" line="46"/>
        <source>Filter</source>
        <translation>Filtr</translation>
    </message>
    <message>
        <location filename="quizsetupdialog.ui" line="52"/>
        <source>Textbook</source>
        <translation>Učebnice</translation>
    </message>
</context>
<context>
    <name>QuizSummary</name>
    <message>
        <location filename="quizsummary.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="quizsummary.ui" line="20"/>
        <source>Quized words</source>
        <translation>Kvízových slovíček</translation>
    </message>
    <message>
        <location filename="quizsummary.ui" line="28"/>
        <source>Correct</source>
        <translation>Správně</translation>
    </message>
    <message>
        <location filename="quizsummary.ui" line="42"/>
        <source>Wrong</source>
        <translation>Špatně</translation>
    </message>
    <message>
        <location filename="quizsummary.ui" line="56"/>
        <source>Total</source>
        <translation>Celkem</translation>
    </message>
    <message>
        <location filename="quizsummary.ui" line="70"/>
        <source>To repeat</source>
        <translation>K opakování</translation>
    </message>
    <message>
        <location filename="quizsummary.ui" line="94"/>
        <source>Repeat</source>
        <translation>Opakovat</translation>
    </message>
    <message>
        <location filename="quizsummary.ui" line="101"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="quizsummary.ui" line="108"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>QuizWordResult</name>
    <message>
        <location filename="quizwordresult.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="quizwordresult.ui" line="20"/>
        <source>Word definition</source>
        <translation>Definice slovíčka</translation>
    </message>
    <message>
        <location filename="quizwordresult.ui" line="36"/>
        <source>Answer</source>
        <translation>Odpověď</translation>
    </message>
    <message>
        <location filename="quizwordresult.ui" line="49"/>
        <source>Correct!</source>
        <translation>Správně!</translation>
    </message>
    <message>
        <location filename="quizwordresult.ui" line="84"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
    <message>
        <location filename="quizwordresult.ui" line="96"/>
        <source>End Quiz</source>
        <translation>Konec kvízu</translation>
    </message>
    <message>
        <location filename="quizwordresult.cpp" line="19"/>
        <source>Wrong!</source>
        <translation>Špatně!</translation>
    </message>
</context>
</TS>
