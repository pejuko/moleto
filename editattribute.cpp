#include "editattribute.h"
#include "ui_editattribute.h"

EditAttribute::EditAttribute(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditAttribute)
{
    ui->setupUi(this);
}

EditAttribute::EditAttribute(QWidget *parent, QString key, QString value) :
    QDialog(parent),
    ui(new Ui::EditAttribute)
{
    ui->setupUi(this);
    ui->key->setText(key);
    ui->value->setText(value);
}

EditAttribute::~EditAttribute()
{
    delete ui;
}

QString EditAttribute::key()
{
    return ui->key->text();
}

QString EditAttribute::value()
{
    return ui->value->text();
}
