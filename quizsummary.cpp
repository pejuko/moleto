#include "quizsummary.h"
#include "ui_quizsummary.h"

QuizSummary::QuizSummary(QWidget *parent, int known, int unknown, int left) :
    QDialog(parent),
    ui(new Ui::QuizSummary)
{
    ui->setupUi(this);

    connect(ui->saveButton, SIGNAL(clicked()), this, SLOT(setResultSave()));
    connect(ui->repeatButton, SIGNAL(clicked()), this, SLOT(setResultRepeat()));
    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(setResultCancel()));

    ui->correct->setText(QString::number(known));
    ui->wrong->setText(QString::number(unknown));
    ui->total->setText(QString::number(known + unknown));
    ui->left->setText(QString::number(left));

    if (unknown == 0 || left == 0) {
        ui->repeatButton->setDisabled(true);
    } else {
        ui->repeatButton->setEnabled(true);
    }
}

QuizSummary::~QuizSummary()
{
    delete ui;
}
