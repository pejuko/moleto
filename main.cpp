#include <QtGui/QApplication>
#include <QLibraryInfo>
#include <QLocale>
#include <QTranslator>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("moleto");
    QCoreApplication::setApplicationName("moleto");

    QApplication a(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    a.installTranslator(&qtTranslator);

    QTranslator moletoTranslator;
    moletoTranslator.load("moleto_" + QLocale::system().name());
    a.installTranslator(&moletoTranslator);

    MainWindow w;

    w.show();

    return a.exec();
}
// vim: expandtab:ts=4:sw=4
