#include <iostream>

using namespace std;

#include <QtGui>
#include <QSqlError>

#include "mainwindow.h"
#include "editword.h"
#include "quizsetupdialog.h"
#include "quizdialog.h"
#include "quickadd.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    db()
{
    ui->setupUi(this);

    ui->actionSave->setEnabled(false);
    //ui->actionEditWord->setEnabled(false);
    //ui->actionDeleteWord->setEnabled(false);
    
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->actionNew, SIGNAL(triggered()), this, SLOT(newFile()));
    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(openFile()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(about()));
    connect(ui->actionEditWord, SIGNAL(triggered()), this, SLOT(editWord()));
    connect(ui->actionAddWord, SIGNAL(triggered()), this, SLOT(addWord()));
    connect(ui->actionDeleteWord, SIGNAL(triggered()), this, SLOT(deleteWord()));
    connect(ui->actionQuiz, SIGNAL(triggered()), this, SLOT(quiz()));
    connect(ui->actionQuickAdd, SIGNAL(triggered()), this, SLOT(quickAdd()));
    connect(ui->table, SIGNAL(wordChanged()), this, SLOT(wordChanged()));
    loadSettings();
    
    ui->table->setModel(db.listWords());
    ui->table->setColumnWidth(1, 50);
    ui->table->setColumnWidth(2, 150);
    ui->table->setColumnWidth(3, 150);
    ui->table->setColumnWidth(4, 50);
    ui->table->setColumnHidden(0, true);
    ui->table->setSortingEnabled(true);
    ui->table->sortByColumn(2, Qt::AscendingOrder);
}

MainWindow::~MainWindow()
{
    saveSettings();
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::reloadWords()
{
    delete ui->table->model();
    ui->table->setModel(db.listWords(ui->filter->text()));
}

void MainWindow::about()
{
    QMessageBox::information(this, tr("about Moleto"), tr("Moleto is a vocabulary learning tool by Petr Kovar (pejuko@gmail.com)."));
}

bool MainWindow::loadFile(QString path)
{
    try {
        db.open(path);
        setWindowTitle("moleto: " + path);
    } catch (QSqlError e) {
        QMessageBox::information(this, tr("error"), e.text());
        QMessageBox::information(this, tr("path"), db.path());
        return false;
    }
    return true;
}

void MainWindow::newFile()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("New file"), "", "");
    if (fileName.isEmpty())
        return;

    db.close();

    if (!loadFile(fileName))
        return;

    reloadWords();
}

void MainWindow::openFile()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("moleto files (*.db);; All Files(*)"));
    if (fileName.isEmpty())
        return;

    db.close();

    if (!loadFile(fileName))
        return;

    reloadWords();
    //QMessageBox::information(this, tr("db version"), QString::number(db.version()));
}

void MainWindow::quiz()
{
    QuizSetupDialog qsd(this, &db);
    if (qsd.exec()) {
        QuizDialog qd(this, &db, qsd.mode(), qsd.textbook());
        if (qd.hasWords()) {
            qd.exec();
            qd.close();
            reloadWords();
        } else {
            QMessageBox::information(this, tr("No words today"), tr("All words were quized. Try it tomorow!"));
        }
    }
}

void MainWindow::loadSettings()
{
    QSettings settings;
    
    QString data_dir = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
    QDir data(data_dir);
    data.mkpath(data_dir);

    QString default_db = data_dir + "/" + "vocabulary.db";
    QString last_db = settings.value("lastdb",  default_db).toString();
    if (last_db != default_db && ! QFile::exists(last_db)) {
        last_db = default_db;
    }

    loadFile(last_db);
}

void MainWindow::saveSettings()
{
    QSettings settings;

    settings.setValue("lastdb", db.path());
}

void MainWindow::addWord()
{
    Word *w = new Word();
    EditWord wd(this, w, &db);
    wd.setWindowTitle(tr("Add New Word"));
    wd.show();
    if (wd.exec()) {
        wd.flushForm();
        db.saveWord(wd.word());
        reloadWords();
        ui->table->selectWord(wd.word());
        qDebug() << "word added";
    }
    delete w;
}

void MainWindow::quickAdd()
{
    QuickAdd qad(this);
    qad.show();
    if (qad.exec()) {
        qad.flushForm();
        db.saveWord(qad.word());
        reloadWords();
        ui->table->selectWord(qad.word());
        qDebug() << "word added";
    }
}

void MainWindow::editWord()
{
    Word *w = db.getWord(ui->table->currentID());
    if (!w)
        return;

    EditWord wd(this, w, &db);
    wd.setWindowTitle(tr("Edit Word"));
    wd.show();
    if (wd.exec()) {
        wd.flushForm();
        db.saveWord(wd.word());
        QModelIndex idx = ui->table->currentIndex();
        reloadWords();
        ui->table->setCurrentIndex(idx);
        qDebug() << "word edited";
    }
    delete w;
}

void MainWindow::deleteWord()
{
    int wordId = ui->table->currentID();
    
    if (QMessageBox::question(this, tr("Delete Word?"), tr("<b>Delete</b> word: ") + ui->table->currentWord() + "?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {
        db.deleteWord(wordId);
        reloadWords();
    }
}

void MainWindow::wordChanged()
{
    int wordId = ui->table->currentID();
    if (wordId) {
        Word *w = db.getWord(wordId);
        ui->details->updateWord(w);
        delete w;
    }
}

// vim: expandtab:ts=4:sw=4
