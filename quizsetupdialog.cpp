#include "quizsetupdialog.h"
#include "ui_quizsetupdialog.h"

QuizSetupDialog::QuizSetupDialog(QWidget *parent, DBManager *dbm) :
    QDialog(parent),
    ui(new Ui::QuizSetupDialog),
    db(dbm)
{
    ui->setupUi(this);
    setWindowTitle(tr("Quiz setup"));

    // fill ui->textbook filter
    ui->textbook->addItem("");
    QStringList items = dbm->textbookList();
    QStringList::iterator it = items.begin();
    for(; it != items.end(); it++) {
        if ((*it).trimmed() == "")
            continue;
        ui->textbook->addItem(*it);
    }
}

QuizSetupDialog::~QuizSetupDialog()
{
    delete ui;
}

QuizSetupDialog::Mode QuizSetupDialog::mode() const
{
    if (ui->flashCardQuiz->isChecked())
        return FlashCard;

    return Normal;
}

QString QuizSetupDialog::textbook() const
{
    return ui->textbook->currentText();
}
