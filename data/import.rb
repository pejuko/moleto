#!/usr/bin/env ruby

if ARGV.size != 3
    puts "import.rb dict stat db"
    exit 1
end


$dict_file = ARGV.shift
$stat_file = ARGV.shift
$db_file = ARGV.shift


require 'rubygems'
require 'sequel'

$db = Sequel.sqlite($db_file)


require "rexml/document"

$dict = REXML::Document.new File.new($dict_file)
$stat = REXML::Document.new File.new($stat_file)


$cmap = { 'pastt' => 'past simple',
          'pastp' => 'past perfect'}

def get(element, child=nil, attrib=nil)
    e = element
    e = element.elements["#{child}"] if child
    return nil if not e
    return e.text if not attrib
    return e.attributes[attrib]
end

$dict.elements.each("//entry") do |entry|
    id = get entry, nil, 'id'
    cl = get entry, nil, 'class'
    writ = get entry, "orth"
    pron = get entry, "pron"
    cat = get entry,"category"
    lesson = get entry, "lesson"

    s = $stat.elements["//entry-info[@entry-id='#{id}']"]
    created = get s, nil, 'date-added'
    batch = get s, nil, 'batch'
    last_quiz = get s, nil, 'last-queried'
    mistakes = get s, nil, 'mistakes'
    queried = get s, nil, 'queried'

    query = "insert into word (written, pronunciation, class, batch, lesson, textbook, last_result, last_quiz, quizes, mistakes) values ('%s', '%s', '%s', %d, '%s', '%s', '%s', '%s', %d, %d)" % [writ, pron, cl, batch.to_i, lesson, cat, "0", last_quiz, queried.to_i, mistakes.to_i]

    puts query
    key = $db[:word].insert :written => writ, :pronunciation => pron, :class => cl, :batch => batch, :lesson => lesson, :textbook => cat, :last_result => "0", :last_quiz => last_quiz, :quizes => queried, :mistakes => mistakes

    entry.elements.each("attr") do |attrib|
        akey = get attrib, nil, 'name'
        avalue = get attrib
        akey = $cmap[akey] if $cmap[akey]
        $db[:attributes].insert :key => akey, :value => avalue, :wordid => key
    end

    entry.elements.each("sense") do |sense|
        sid = get sense, nil, 'id'
        trans = get sense, 'trans'
        definition = get sense, 'def'

        mkey = $db[:meaning].insert :wordid => key, :definition => definition
        mtkey = $db[:meaning_translations].insert :meaningid => mkey, :translation => trans
        $dict.elements.each("//example/ex/link[@sid='#{sid}']") do |link|
            original = link.parent.to_s.gsub(/<[^>]*>/, '')
            translation = get link.parent.parent, "tr"
            $db[:examples].insert :meaningid => mkey, :original => original, :translation => translation
        end
    end
end

$db.disconnect
