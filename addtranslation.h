#ifndef ADDTRANSLATION_H
#define ADDTRANSLATION_H

#include <QDialog>
#include <QLineEdit>

namespace Ui {
class AddTranslation;
}

class AddTranslation : public QDialog
{
    Q_OBJECT
    
public:
    explicit AddTranslation(QWidget *parent = 0);
    ~AddTranslation();
    
    QString translation() const;
    void setTranslation(QString text);
    
private:
    Ui::AddTranslation *ui;
};

#endif // ADDTRANSLATION_H
