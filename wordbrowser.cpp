#include "wordbrowser.h"
#include "word.h"

WordBrowser::WordBrowser(QWidget *parent) :
    QTextBrowser(parent)
{
}

void WordBrowser::updateWord(Word *w)
{
    if (! w)
        return;
    setHtml(w->toHtml());
    update();
}
