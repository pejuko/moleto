#ifndef QUIZSETUPDIALOG_H
#define QUIZSETUPDIALOG_H

#include <QDialog>

#include "dbmanager.h"

namespace Ui {
class QuizSetupDialog;
}

class QuizSetupDialog : public QDialog
{
    Q_OBJECT
    
public:
    enum Mode { Normal, FlashCard };

    explicit QuizSetupDialog(QWidget *parent = 0, DBManager *dbm = 0);
    ~QuizSetupDialog();

    Mode mode() const;
    QString textbook() const;
    
private:
    Ui::QuizSetupDialog *ui;
    DBManager *db;
};

#endif // QUIZSETUPDIALOG_H
