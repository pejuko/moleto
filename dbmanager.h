#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QtSql/QSqlDatabase>
#include <QSqlQueryModel>
#include <QStringList>

#include "word.h"

class DBManager
{
public:
    DBManager();
    void open(QString path);
    void close();

    QString path() { return m_db.databaseName(); };
    int version() const { return m_version; };
    
    QSqlQueryModel *listWords(QString filter="", int sortby_column=3);
    void deleteWord(int id);
    
    Word *getWord(int wordId);
    bool saveWord(Word *w, bool transaction=true);
    bool saveWords(QList<Word *> &words);
    
    Meaning *getMeaning(Word *w, int meaningId);
    
    QStringList list(QString field) const;
    QStringList lessonList() const { return list("lesson"); };
    QStringList textbookList() const { return list("textbook"); };
    QStringList classList() const { return list("class"); };

    QList<Word *> getQuizWords();
protected:
    void migrate();

private:
    QSqlDatabase m_db;
    int m_version;
};

#endif // DBMANAGER_H
