#include "dbmanager.h"
#include "editword.h"
#include "editattribute.h"
#include "editmeaning.h"
#include "word.h"
#include "ui_editword.h"

#include <QtCore>
#include <QMessageBox>

EditWord::EditWord(QWidget *parent, Word *w, DBManager *dbm) :
    QDialog(parent),
    ui(new Ui::EditWord),
    db(dbm),
    p_word(w)
{
    ui->setupUi(this);
    
    connect(ui->addAttribute, SIGNAL(clicked()), this, SLOT(addAttribute()));
    connect(ui->editAttribute, SIGNAL(clicked()), this, SLOT(editAttribute()));
    connect(ui->deleteAttribute, SIGNAL(clicked()), this, SLOT(deleteAttribute()));

    fillForm();

    if (ui->written->text().trimmed().isEmpty())
        ui->written->setFocus();
}

EditWord::~EditWord()
{
    delete ui;
}

void EditWord::flushForm()
{
    p_word->textbook = ui->textbook->currentText();
    p_word->lesson = ui->lesson->currentText();
    p_word->written = ui->written->text();
    p_word->pronunciation = ui->pronunciation->text();
    p_word->wordClass = ui->wordClass->currentText();
    p_word->batch = 0;
    p_word->comment = ui->comment->toPlainText();
}

void EditWord::addMeaning()
{
    Meaning *m = new Meaning(p_word);
    EditMeaning md(this, m, db);
    md.setWindowTitle(tr("Add new meaning"));
    md.show();
    flushForm();
    if (md.exec()) {
        md.flushForm();
        p_word->meanings.append(m);
        fillMeanings();
    }
}

void EditWord::editMeaning()
{
    int row = ui->meanings->currentRow();
    if (row < 0)
        return;

    Meaning *m = p_word->meanings.at(row);
    EditMeaning md(this, m, db);
    md.fillForm();
    md.setWindowTitle(tr("Edit meaning"));
    md.show();
    flushForm();
    if (md.exec()) {
        md.flushForm();
        fillMeanings();
    }
}

void EditWord::deleteMeaning()
{
    int row = ui->meanings->currentRow();
    if (row < 0)
        return;

    QString msg = tr("<b>Delete</b> meaning: ") + ui->meanings->currentItem()->text() + "?";

    flushForm();
    if (QMessageBox::question(this, tr("Delete Meaning?"), msg, QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {
        delete p_word->meanings.at(row);
        p_word->meanings.removeAt(row);
        fillMeanings();
    }
}

void EditWord::addAttribute()
{
    EditAttribute ad(this);

    flushForm();

    ad.show();
    if (ad.exec()) {
        p_word->attributes[ad.key()] = ad.value();
        fillAttributes();
    }
}

void EditWord::editAttribute()
{
    int r = ui->listAttributes->currentRow();
    if (r < 0)
        return;

    QString key = ui->listAttributes->item(r,0)->text();
    QString value = ui->listAttributes->item(r,1)->text();
    EditAttribute ad(this, key, value);

    flushForm();

    ad.show();
    if (ad.exec()) {
        p_word->attributes.remove(key);
        p_word->attributes[ad.key()] = ad.value();
        fillAttributes();
    }
}

void EditWord::deleteAttribute()
{
    int r = ui->listAttributes->currentRow();
    if (r < 0)
        return;

    QString key = ui->listAttributes->item(r,0)->text();

    flushForm();
    p_word->attributes.remove(key);
    fillAttributes();
}

void EditWord::fillMeanings()
{
    ui->meanings->clear();
    QList<Meaning *>::iterator it = p_word->meanings.begin();
    while (it != p_word->meanings.end()) {
        QListWidgetItem *item = new QListWidgetItem();
        QString m;

        m = (*it)->definition;
        m += " (";
        QList<QString>::iterator i = (*it)->translations.begin();
        while (i != (*it)->translations.end()) {
            if (i != (*it)->translations.begin())
                m += ", ";
            m += (*i);
            ++i;
        }
        m += ")";

        item->setText(m);
        ui->meanings->addItem(item);
        ++it;
    }
}

void EditWord::fillAttributes()
{
    ui->listAttributes->clearSpans();
    ui->listAttributes->setColumnCount(2);
    ui->listAttributes->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Key")));
    ui->listAttributes->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Value")));
    ui->listAttributes->setRowCount(p_word->attributes.count());
    QMap<QString, QString>::iterator ait = p_word->attributes.begin();
    for (int r = 0; ait != p_word->attributes.end(); r++, ait++) {
        ui->listAttributes->setItem(r, 0, new QTableWidgetItem(ait.key()));
        ui->listAttributes->setItem(r, 1, new QTableWidgetItem(ait.value()));
    }
}

void EditWord::fillForm()
{
    int idx;

    // setup textbook list
    ui->textbook->clear();
    ui->textbook->addItems(db->textbookList());
    idx = ui->textbook->findText(p_word->textbook);
    if (idx > 0) ui->textbook->setCurrentIndex(idx);

    // setup lessons list
    ui->lesson->clear();
    ui->lesson->addItems(db->lessonList());
    idx = ui->lesson->findText(p_word->lesson);
    if (idx > 0) ui->lesson->setCurrentIndex(idx);

    // setup word class list
    ui->wordClass->clear();
    ui->wordClass->addItems(db->classList());
    idx = ui->wordClass->findText(p_word->wordClass);
    if (idx > 0) ui->wordClass->setCurrentIndex(idx);

    ui->written->setText(p_word->written);
    ui->pronunciation->setText(p_word->pronunciation);

    // setup meanings
    fillMeanings();

    // setup attributes
    fillAttributes();

    // setup comment
    ui->comment->setPlainText(p_word->comment);
}
