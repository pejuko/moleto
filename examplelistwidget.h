#ifndef EXAMPLELISTWIDGET_H
#define EXAMPLELISTWIDGET_H

#include <QListWidget>

#include "word.h"

class ExampleListWidget : public QListWidget
{
    Q_OBJECT
public:
    explicit ExampleListWidget(QWidget *parent = 0);
    
    void addItems(Meaning *meaning);
    
signals:
    
public slots:
    
};

#endif // EXAMPLELISTWIDGET_H
