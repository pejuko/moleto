#ifndef QUICKADD_H
#define QUICKADD_H

#include <QDialog>

#include "word.h"

namespace Ui {
class QuickAdd;
}

class QuickAdd : public QDialog
{
    Q_OBJECT
    
public:
    explicit QuickAdd(QWidget *parent = 0);
    ~QuickAdd();

    void flushForm();
    Word *word() { return m_word; }
    
private:
    Ui::QuickAdd *ui;
    Word *m_word;
};

#endif // QUICKADD_H
