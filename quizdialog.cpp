#include "quizdialog.h"
#include "quizsetupdialog.h"
#include "quizwordresult.h"
#include "quizsummary.h"
#include "ui_quizdialog.h"

#include <QDateTime>
#include <QMessageBox>

QuizDialog::QuizDialog(QWidget *parent, DBManager *dbm, QuizSetupDialog::Mode mode, QString textbook) :
    QDialog(parent),
    ui(new Ui::QuizDialog),
    db(dbm),
    m_mode(mode),
    m_textbook(textbook),
    m_words(dbm->getQuizWords()),
    m_unknownWords(),
    m_known(0),
    m_unknown(0),
    m_repeat(false),
    m_actKnown(0),
    m_actUnknown(0)
{
    ui->setupUi(this);

    connect(ui->knowButton, SIGNAL(clicked()), this, SLOT(knownWord()));
    connect(ui->dontButton, SIGNAL(clicked()), this, SLOT(unknownWord()));

    m_it = m_words.begin();

    // apply textbook filter and erase words
    if (! m_textbook.isEmpty()) {
        while (m_it != m_words.end()) {
            Word *w = *m_it;
            if (w->textbook != m_textbook) {
                delete w;
                m_it = m_words.erase(m_it);
            } else {
                ++m_it;
            }
        }
        m_it = m_words.begin();
    }

    m_actWords = m_words.size();
    if (m_actWords > 0) {
        showWord();
    }
}

QuizDialog::~QuizDialog()
{
    // save words to db
    m_it = m_words.begin();
    for (; m_it != m_words.end(); m_it++) {
        delete *m_it;
    }
    m_words.clear();

    db = NULL;
    delete ui;
}

// save words to db
void QuizDialog::save()
{
    QDateTime date = QDateTime::currentDateTime();

    db->saveWords(m_words);

}

void QuizDialog::showWord()
{
    if (m_it == m_words.end())
        return;

    if (m_mode == QuizSetupDialog::FlashCard) {
        ui->answer->hide();
    } else {
        ui->answer->setFocus();
    }

    ui->question->setText((*m_it)->meaningsToHtml(false));

    setWindowTitle(tr("word ") + QString::number(m_actKnown + m_actUnknown + 1) + "/" + QString::number(m_actWords));

    emit update();
}

void QuizDialog::knownWord()
{
    if (m_it != m_words.end()) {
        Word *w = *m_it;
        if (m_mode == QuizSetupDialog::Normal && !w->compare(ui->answer->text())) {
            unknownWord();
            return;
        } else if (! m_repeat) {
            m_actKnown += 1;
            w->last_result = "1";
            w->batch += 1;
        } else {
            // repeat known
            m_actKnown += 1;
            m_unknownWords.removeOne(w);
        }
    }

    nextWord();
}

void QuizDialog::unknownWord()
{
    if (m_it != m_words.end()) {
        Word *w = *m_it;
        if (! m_repeat) {
            m_actUnknown += 1;
            w->mistakes += 1;
            w->last_result = "0";
            w->batch = 0;
            if (m_unknownWords.indexOf(w) == -1)
                m_unknownWords.push_back(*m_it);
        } else {
            m_actUnknown += 1;
        }
    }

    nextWord();
}

void QuizDialog::nextUnknown()
{
    while (m_it != m_words.end() && m_unknownWords.indexOf(*m_it) == -1)
        m_it++;
}

void QuizDialog::nextWord()
{
    bool should_continue = false;

    if (m_it != m_words.end()) {
        Word *w = (*m_it);

        if (!m_repeat) {
            w->quizes += 1;
            w->last_quiz = QDateTime::currentDateTime();
        }

        // show result
        QString answer = ui->answer->text();
        bool correct = w->compare(answer);
        QuizWordResult wrd(this, w, answer, m_mode, correct);
        wrd.setModal(true);
        wrd.setWindowTitle(tr("word ") + QString::number(m_actKnown + m_actUnknown) + "/" + QString::number(m_actWords));
        wrd.show();
        should_continue = wrd.exec();
        ui->answer->setText("");

        m_it++;
        if (m_repeat)
            nextUnknown();
    }

    // next word or summary
    if (m_it == m_words.end() || ! should_continue) {
        if (!m_repeat) {
            m_known = m_actKnown;
            m_unknown = m_actUnknown;
        }
        m_repeat = false;
        // it was last word
        QuizSummary sd(this, m_known, m_unknown, m_unknownWords.size());
        sd.setModal(true);
        sd.show();
        switch(sd.exec()) {
            case QuizSummary::Save:
                save();
                done(true);
                break;
            case QuizSummary::Cancel:
                done(false);
                break;
            default:
                //repeat
                m_repeat = true;
                m_it = m_words.begin();
                m_actKnown = 0;
                m_actUnknown = 0;
                m_actWords = m_unknownWords.size();
                nextUnknown();
                showWord();
                break;
        }
        sd.close();
    } else {
        // show next word
        showWord();
    }
}
