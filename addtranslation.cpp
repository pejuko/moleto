#include "addtranslation.h"
#include "ui_addtranslation.h"

AddTranslation::AddTranslation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddTranslation)
{
    ui->setupUi(this);
}

AddTranslation::~AddTranslation()
{
    delete ui;
}

QString AddTranslation::translation() const
{
    return ui->translation->text();
}

void AddTranslation::setTranslation(QString text)
{
    ui->translation->setText(text);
}
