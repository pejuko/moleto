#include "quizwordresult.h"
#include "ui_quizwordresult.h"

QuizWordResult::QuizWordResult(QWidget *parent, Word *w, QString const& answer, QuizSetupDialog::Mode mode, bool correct) :
    QDialog(parent),
    ui(new Ui::QuizWordResult),
    m_pWord(w),
    m_correct(correct),
    m_mode(mode)
{
    ui->setupUi(this);

    connect(ui->continueButton, SIGNAL(clicked()), this, SLOT(doneContinue()));
    connect(ui->endQuiz, SIGNAL(clicked()), this, SLOT(doneEnd()));

    ui->wordDefinition->setText(toHtml());
    ui->answer->setText(answer);

    if (m_mode == QuizSetupDialog::FlashCard)
        ui->answerBox->hide();
    else if (! m_correct)
        ui->label->setText(tr("Wrong!"));
}

QuizWordResult::~QuizWordResult()
{
    delete ui;
}

QString QuizWordResult::toHtml()
{
    return m_pWord->toHtml();
}
