#ifndef WORDLIST_H
#define WORDLIST_H

#include <QTableView>

#include "word.h"

class WordList : public QTableView
{
    Q_OBJECT
public:
    explicit WordList(QWidget *parent = 0);
    ~WordList();
    
    int currentID();
    QString currentWord();
    void selectWord(Word *w);

signals:
    void wordChanged();
    
public slots:

protected:
    virtual void currentChanged( const QModelIndex & current, const QModelIndex & previous );
    
};

#endif // WORDLIST_H
