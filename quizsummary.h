#ifndef QUIZSUMMARY_H
#define QUIZSUMMARY_H

#include <QDialog>

namespace Ui {
class QuizSummary;
}

class QuizSummary : public QDialog
{
    Q_OBJECT
    
public:
    enum Result { Save, Repeat, Cancel };

    explicit QuizSummary(QWidget *parent = 0, int known = 0, int unknown = 0, int left=0);
    ~QuizSummary();

public slots:
    void setResultSave() { done(Save); }
    void setResultRepeat() { done(Repeat); }
    void setResultCancel() { done(Cancel); }
    
private:
    Ui::QuizSummary *ui;

};

#endif // QUIZSUMMARY_H
