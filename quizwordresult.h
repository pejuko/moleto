#ifndef QUIZWORDRESULT_H
#define QUIZWORDRESULT_H

#include <QDialog>

#include "word.h"
#include "quizsetupdialog.h"

namespace Ui {
class QuizWordResult;
}

class QuizWordResult : public QDialog
{
    Q_OBJECT
    
public:
    explicit QuizWordResult(QWidget *parent = 0, Word *w=0, const QString &answer="", QuizSetupDialog::Mode mode=QuizSetupDialog::Normal, bool correct=false);
    ~QuizWordResult();

    QString toHtml();

public slots:
    void doneContinue() { done(true); }
    void doneEnd() { done(false); }
    
private:
    Ui::QuizWordResult *ui;
    Word *m_pWord;
    bool m_correct;
    QuizSetupDialog::Mode m_mode;
};

#endif // QUIZWORDRESULT_H
