#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql/QSqlDatabase>

#include "dbmanager.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void loadSettings();
    void saveSettings();
    
    bool loadFile(QString path);

public slots:
    void newFile();
    void openFile();
    void quiz();
    void about();
    void addWord();
    void quickAdd();
    void editWord();
    void deleteWord();
    void wordChanged();
    void reloadWords();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MainWindow *ui;
    DBManager db;

};

#endif // MAINWINDOW_H
// vim: expandtab:ts=4:sw=4
